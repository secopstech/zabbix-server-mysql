# Zabbix-server-mysql

This image forked from the official zabbix repo ( https://hub.docker.com/r/zabbix/zabbix-server-mysql/ )
and it has some customizations for docker swarm mode. 

With this image you can use MYSQL_ROOT_PASSWORD_FILE and MYSQL_PASSWORD_FILE variables to supply the 
credentials as a secret within swarm cluster.

i.e:

```
docker service create --name some-zabbix-server-mysql \
 -e DB_SERVER_HOST="some-mysql-server" \
 -e MYSQL_ROOT_PASSWORD_FILE="/run/secret/mysql_root_passwd" \
 -e MYSQL_USER="some-user" \
 -e MYSQL_PASSWORD_FILE="/run/secret/mysql_passwd" \
 secopstech/zabbix-server-mysql:latest
```

It also supports default MYSQL_ROOT_PASSWORD and MYSQL_PASSWORD variables for standalone mode as follows:

```
docker run --name some-zabbix-server-mysql \
 -e DB_SERVER_HOST="172.17.0.2" \
 -e MYSQL_ROOT_PASSWORD="some-root-password" \
 -e MYSQL_USER="some-user" \
 -e MYSQL_PASSWORD="some-other-password" \
 -d secopstech/zabbix-server-mysql:latest

```
